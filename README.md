# Local GitLab runner

Creates a local gitlab runner for your projects

## How does it work?

Run `./Taskfile start` to create your own local GitLab runner. You need to have docker installed locally.

Run `./Taskfile register` to register a personal runner to either your project, group or GitLab instance.

Edit the `config/config.toml` file if you want to add more settings. This file is not committed.

## Fixing networking issues

When running the GitLab runner, you could experience networking issues (like web pages not loading). This can be fixed
by changing your default network in your docker settings.

Add the following to your `/etc/docker/daemon.conf`:

```json
{
  "bip": "192.168.60.1/24",
  "default-address-pools": [
    {
      "base": "10.17.0.1/16",
      "size": 24
    }
  ]
}
```
